import axios from 'axios';
import setAuthToken from '../components/utils/setAuthToken';
import {
  GET_CONTACTS, GET_USER, SET_LOADING, CONTACT_ERROR,
} from './types';

export const setLoading = () => (dispatch) => {
  dispatch({
    type: SET_LOADING,
  });
};

// Load User
export const getContacts = () => async (dispatch) => {
  try {
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }
    setLoading();
    const res = await axios.get('/contacts');
    dispatch({
      type: GET_CONTACTS,
      payload: res.data,
    });
  } catch (err) {
    dispatch({
      type: CONTACT_ERROR,
      payload: err.response.msg,
    });
  }
};

export const getUser = (id) => async (dispatch) => {
  setLoading();
  const res = await axios.get(`/contacts/${id}`);
  dispatch({
    type: GET_USER,
    payload: res.data,
  });
};
