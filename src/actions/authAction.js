import axios from 'axios';
import {
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  SET_ALERT,
  REMOVE_ALERT,

} from './types';


// Load User
// export const loadUser = async () => {
//   // if (localStorage.token) {
//   //   setAuthToken(localStorage.token);
//   // }
//   store.dispatch({ type: SET_LOADING });
//   try {
//     const res = await axios.get('http://localhost:5000/api/auth');

//     store.dispatch({
//       type: USER_LOADED,
//       payload: res.data,
//     });
//   } catch (err) {
//     store.dispatch({ type: AUTH_ERROR });
//     store.dispatch({
//       type: SET_ALERT,
//       payload: err.response.data.msg,
//     });
//     setTimeout(() => store.dispatch({ type: REMOVE_ALERT }), 3000);
//   }
// };

export const login = ({ email, password }) => async (dispatch) => {
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const userData = {
    username: email,
    password,
  };

  try {
    const res = await axios.post('/auth/login', userData, config);
    dispatch({
      type: LOGIN_SUCCESS,
      payload: res.data.token,
    });
  } catch (error) {
    dispatch({
      type: LOGIN_FAIL,
    });

    dispatch({
      type: SET_ALERT,
      payload: error.response.data.error_description,
    });

    setTimeout(() => dispatch({ type: REMOVE_ALERT }), 3000);
  }
};

export const register = ({ email, password }) => (dispatch) => {
  // Headers
  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Request body
  const body = {
    username: email,
    password,
  };

  axios
    .post('auth/register', body, config)
    .then((res) => dispatch({
      type: REGISTER_SUCCESS,
      payload: res.status,
    }))
    .catch((err) => {
      dispatch({
        type: REGISTER_FAIL,
      });
      dispatch({
        type: SET_ALERT,
        payload: err.response.data.msg,
      });

      setTimeout(() => dispatch({ type: REMOVE_ALERT }), 3000);
    });
};

export const logout = () => (dispatch) => {
  dispatch({
    type: LOGOUT_SUCCESS,
  });
};
