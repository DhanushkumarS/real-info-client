/* eslint-disable import/extensions */
/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { getContacts, getUser } from '../../actions/contactAction';
import { logout } from '../../actions/authAction';
import Header from './Header';
import UserList from './UserList';

const UserDashboard = ({
  user,
  contacts,
  getContacts,
  history,
  // getUser,
  logout,
  isLoading,
}) => {
  useEffect(() => {
    getContacts();
    // eslint-disable-next-line
  }, []);

  if (!localStorage.token) {
    history.push('/');
  }

  const Logout = () => {
    logout();
    history.push('/');
  };

  const DetailPage = (id) => {
    history.push(`/user/${id}`);
  };

  if (isLoading) {
    return (
      <div
        className="spinner-border"
        style={{ width: '3rem', height: '3rem' }}
        role="status"
      >
        <span className="sr-only">Loading...</span>
      </div>
    );
  }

  const Loading = (
    <div
      className="spinner-border"
      style={{ width: '3rem', height: '3rem', margin: '100px auto' }}
      role="status"
    >
      <span className="sr-only">Loading...</span>
    </div>
  );

  const AddUser = () => {
    history.push('/adduser');
  };

  return (
    <div>
      <Header user={user} Logout={Logout} />
      {contacts ? (
        <UserList contacts={contacts} DetailPage={DetailPage} />
      ) : (
        Loading
      )}
      {contacts && (
        <div className="container">
          <div
            className="btn btn-dark"
            role="button"
            tabIndex={0}
            onClick={AddUser}
            onKeyDown={AddUser}
          >
            Add User
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.login.user,
  contacts: state.contact.contacts,
  isLoading: state.login.isLoading,
  error: state.login.error,
});

export default compose(
  withRouter,
  connect(mapStateToProps, {
    getContacts,
    getUser,
    logout,
  }),
)(UserDashboard);
