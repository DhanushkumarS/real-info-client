import React from 'react';

const AddUser = () => (
  <div className="container">
    <div className="row">
      <div className="col-md-6 mx-auto">
        <div className="card">
          <div className="card-header">
            <h4>Add User</h4>
          </div>
          <div className="card-body">
            <form>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input type="name" name="name" className="form-control" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input type="email" name="email" className="form-control" />
              </div>
              <input
                type="submit"
                value="Add User"
                className="btn btn-dark btn-block"
              />
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default AddUser;
