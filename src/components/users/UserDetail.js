/* eslint-disable no-tabs */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { getUser } from '../../actions/contactAction';

const UserDetail = ({
  userData, history, getUser, match,
}) => {
  const [user, setUser] = useState({
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
  });

  useEffect(() => {
    getUser(match.params.id);
    if (userData != null) {
      setUser({
        ...user,
        firstName: userData.firstName,
        lastName: userData.lastName,
        phone: userData.phone,
        email: userData.email,
      });
    }
    // eslint-disable-next-line
  }, []);

  const {
    firstName, lastName, phone, email,
  } = user;

  const onChange = (e) => setUser({ ...user, [e.target.name]: e.target.value });

  return (
    <div>
      <section id="actions" className="py-4 mb-4 bg-light">
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <a
                role="button"
                onClick={() => {
                  history.push('/user');
                }}
                tabIndex={0}
                className="btn btn-light btn-block"
              >
                <i className="fas fa-arrow-left" />
                {' '}
Back To Dashboard
              </a>
            </div>
            <div className="col-md-3">
              <a href="!#" className="btn btn-dark btn-block">
                <i className="fas fa-check" />
                {' '}
Save
              </a>
            </div>
            <div className="col-md-3">
              <a href="!#" className="btn btn-danger btn-block">
                <i className="fas fa-trash" />
                {' '}
Delete
              </a>
            </div>
          </div>
        </div>
      </section>
      <section id="details">
        <div className="container">
          <div className="row">
            <div className="col">
              <div className="card">
                <div className="card-header">
                  <h4>Edit User</h4>
                </div>
                <div className="card-body">
                  <form>
                    <div className="form-group">
                      <label htmlFor="name">First Name</label>
                      <input
                        type="name"
                        name="name"
                        value={firstName}
                        onChange={onChange}
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="email">Last Name</label>
                      <input
                        type="email"
                        name="email"
                        value={lastName}
                        onChange={onChange}
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="phone">Phone</label>
                      <input
                        type="text"
                        name="phone"
                        value={phone}
                        onChange={onChange}
                        className="form-control"
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="type">Email</label>
                      <input
                        type="text"
                        name="type"
                        value={email}
                        onChange={onChange}
                        className="form-control"
                      />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

UserDetail.propTypes = {
  userData: PropTypes.isRequired,
  history: PropTypes.isRequired,
  getUser: PropTypes.func.isRequired,
  match: PropTypes.isRequired,
};

const mapStateToProps = (state) => ({
  userData: state.contact.user,
});

export default compose(
  withRouter,
  connect(mapStateToProps, { getUser }),
)(UserDetail);
