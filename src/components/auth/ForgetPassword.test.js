import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';
import ForgetPassword from './ForgetPassword';

configure({ adapter: new Adapter() });

describe('<ForgetPassword /> component', () => {
  const container = shallow(<ForgetPassword />);

  it('should have a h4 element', () => {
    expect(container.find('h4').length).toEqual(1);
  });
  it('should have a h4 with text Forget Password', () => {
    expect(container.find('h4').text()).toEqual('Forget Password');
  });

  it('should have an email field', () => {
    expect(container.find('input[type="email"]').length).toEqual(1);
  });

  it('should have proper props for email field', () => {
    expect(container.find('input[type="email"]').props()).toEqual({
      className: 'form-control',
      onChange: expect.any(Function),
      name: 'email',
      type: 'email',
      value: '',
    });
  });

  it('should have sumbit button', () => {
    expect(container.find('input[type="submit"]').length).toEqual(1);
  });

  it('should have proper props for sumbit button', () => {
    expect(container.find('input[type="submit"]').props()).toEqual({
      className: 'btn btn-dark btn-block',
      type: 'submit',
      value: 'Reset Password',
    });
  });

  //   it('should have a password field', () => { /* Similar to above */ });
  // eslint-disable-next-line max-len
  //   it('should have proper props for password field', () => { /* Trimmed for less lines to read */ });
  //   it('should have a submit button', () => { /* */ });
  //   it('should have proper props for submit button', () => { /* */ });
});
