import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';
import { register } from '../../actions/authAction';
import {
  REGISTER_HEADER,
  // NAME_FIELD_TEXT,
  EMAIL_FIELD_TEXT,
  PASSWORD_FIELD_TEXT,
  // CONFIRM_PASSWORD_FIELD_TEXT,
  REGISTER_BUTTON_TEXT,
} from './Constants';
import Alert from './Alert';

const Register = ({
  register, isRegistered, history, error,
}) => {
  const [user, setUser] = useState({
    email: 'dhanushkumarstudy@gmail.com',
    password: 'S*dk1995',
  });

  useEffect(() => {
    if (isRegistered) {
      history.push('/');
    }

    // eslint-disable-next-line
  }, [isRegistered, history]);

  const { email, password } = user;

  const onChange = (e) => setUser({ ...user, [e.target.name]: e.target.value });

  const onSubmit = (e) => {
    e.preventDefault();
    if (email === '' || password === '') {
      // alert('Please fill in all fields', 'danger');
    } else {
      register({
        email,
        password,
      });
    }

    setUser({
      ...user,
      email: '',
      password: '',
    });
  };

  // if (isRegistered) {
  //   return (
  //     <div>
  //       <section id="register">
  //         <div className="bg">
  //           <div className="container">
  //             <div className="row">
  //               <div className="col-md-6 mx-auto">
  //                 <div className="card">
  //                   <div className="card-header">
  //                     <h4>A confirmation email sent..</h4>
  //                     <Link to="/" className="btn btn-dark btn-block">Please Login</Link>
  //                   </div>
  //                 </div>
  //               </div>
  //             </div>
  //           </div>
  //         </div>
  //       </section>
  //     </div>
  //   );
  // }

  return (
    <div>
      <section id="register">
        <div className="bg">
          <div className="container">
            <div className="row">
              <div className="col-md-6 mx-auto">
                <div className="card">
                  <div className="card-header">
                    <h4>{REGISTER_HEADER}</h4>
                  </div>
                  <div className="card-body">
                    {error && (
                      <div className="alert alert-danger" role="alert">
                        <Alert />
                      </div>
                    )}
                    <form onSubmit={onSubmit}>
                      <div className="form-group">
                        <label htmlFor="password">{EMAIL_FIELD_TEXT}</label>
                        <input
                          type="email"
                          name="email"
                          value={email}
                          onChange={onChange}
                          className="form-control"
                        />
                      </div>
                      <div className="form-group">
                        <label htmlFor="password">{PASSWORD_FIELD_TEXT}</label>
                        <input
                          type="password"
                          name="password"
                          value={password}
                          onChange={onChange}
                          className="form-control"
                        />
                      </div>
                      <input
                        type="submit"
                        value={REGISTER_BUTTON_TEXT}
                        className="btn btn-dark btn-block"
                      />
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

Register.propTypes = {
  register: PropTypes.func.isRequired,
  isRegistered: PropTypes.bool.isRequired,
  history: PropTypes.isRequired,
  error: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  isRegistered: state.login.isRegistered,
  error: state.login.error,
});

export default connect(mapStateToProps, { register })(Register);
