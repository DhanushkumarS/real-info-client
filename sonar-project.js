/* eslint-disable import/no-extraneous-dependencies */
const sonarqubeScanner = require('sonarqube-scanner');

sonarqubeScanner({
  options: {
    'sonar.projectKey': 'sonar-reactapp',
    'sonar.projectName': 'sonar-reactapp',
    'sonar.projectVersion': '1.0',
    'sonar.sources': 'src',
  },
}, () => {});
