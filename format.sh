#!/usr/bin/env sh
echo "Starting lint scanner..."

npx eslint --fix '**/*.js' --ignore-pattern node_modules/

