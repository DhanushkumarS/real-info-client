# License Service UI built using React

This folder is the license application's frontend UI - using React.

## Overview of Stack
  - React v 16
- Testing
  - enzyme

## Setup

1. Install the following:
  - [NodeJs](https://nodejs.org)
  - [Docker](https://docs.docker.com/engine/installation/)
2. Run the following commands to install the prerequisites:
  `npm i`

## Running the app

To start the app for development, run this command: `./start.sh`. This will start the development server on http://localhost:3000

## Formatting the code

To format the code, run the following command: `./format.sh`

## Running the tests

To run all the unit tests, run this command: `./test.sh`

When `./test.sh` is run, "COVERAGE" folder is created in the Project directory with the HTML Report in Coverage > Icov-report >index.html

## `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
